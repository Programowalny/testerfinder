package pl.dgrzankowski.testerfinder.service;


import org.junit.Test;
import org.junit.runner.RunWith;
import static org.assertj.core.api.Assertions.assertThat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import pl.dgrzankowski.testerfinder.dao.SearchRepository;
import pl.dgrzankowski.testerfinder.model.domian.Bug;
import pl.dgrzankowski.testerfinder.model.domian.Device;
import pl.dgrzankowski.testerfinder.model.domian.Tester;
import pl.dgrzankowski.testerfinder.model.dto.ResultDto;
import pl.dgrzankowski.testerfinder.model.dto.SearchCriteriaDTO;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchServiceTest {

    @MockBean
    private SearchRepository searchRepository;

    @Autowired
    private SearchService searchService;

    @Test
    public void shouldReturnDeviceNames(){

        List<Device> deviceList = createTempDeviceList();

        when(searchRepository.getDevices()).thenReturn(deviceList);
        List<String> searchResult = searchService.getDevicesNames();
        assertThat(searchResult).isEqualTo(List.of("iPhone1", "iPhone2", "iPhone3"));
    }

    @Test
    public void shouldReturnCountries(){

        List<Tester> testerList = createTempTesterList();

        when(searchRepository.getTesters()).thenReturn(testerList);
        List<String> searchResult = searchService.getCountryNames();
        assertThat(searchResult).isEqualTo(List.of("US", "GB"));
    }

    @Test
    public void shouldCreateSearchResult(){
        SearchCriteriaDTO criteriaDTO = new SearchCriteriaDTO();
        criteriaDTO.setCountries(List.of("US", "PL"));
        criteriaDTO.setDevices(List.of("iPhone1", "iPhone2"));

        List<Device> deviceList = createTempDeviceList();
        List<Bug> bugList = createTempBugList();
        List<Tester> testerList = createTempTesterList();
        
        when(searchRepository.getDevices()).thenReturn(deviceList);
        when(searchRepository.getBugs()).thenReturn(bugList);
        when(searchRepository.getTesters()).thenReturn(testerList);
        List<ResultDto> searchResult = searchService.createSearchResult(criteriaDTO);
        assertThat(searchResult.get(0)).isEqualTo(new ResultDto("John Grey", 2));
        assertThat(searchResult.get(1)).isEqualTo(new ResultDto("Michael Black", 1));
    }

    private List<Device> createTempDeviceList(){
        Device device1 = new Device();
        device1.setDeviceId(1);
        device1.setDescription("iPhone1");
        Device device2 = new Device();
        device2.setDeviceId(2);
        device2.setDescription("iPhone2");
        Device device3 = new Device();
        device3.setDeviceId(3);
        device3.setDescription("iPhone3");
        List<Device> resultList = new ArrayList<>();
        resultList.add(device1);
        resultList.add(device2);
        resultList.add(device3);
        return resultList;
    }

    private List<Tester> createTempTesterList(){
        Tester tester1 = new Tester();
        tester1.setTesterId(1);
        tester1.setFirstName("John");
        tester1.setLastName("Grey");
        tester1.setCountry("US");
        Tester tester2 = new Tester();
        tester2.setTesterId(2);
        tester2.setFirstName("Michael");
        tester2.setLastName("Black");
        tester2.setCountry("US");
        Tester tester3 = new Tester();
        tester3.setTesterId(3);
        tester3.setFirstName("Steve");
        tester3.setLastName("White");
        tester3.setCountry("US");
        Tester tester4 = new Tester();
        tester4.setTesterId(4);
        tester4.setFirstName("Scott");
        tester4.setLastName("Blue");
        tester4.setCountry("GB");
        List<Tester> resultList = new ArrayList<>();
        resultList.add(tester1);
        resultList.add(tester2);
        resultList.add(tester3);
        resultList.add(tester4);
        return resultList;
    }

    private List<Bug> createTempBugList(){
        Bug bug1 = new Bug();
        bug1.setBugId(1);
        bug1.setDeviceId(1);
        bug1.setTesterId(1);
        Bug bug2 = new Bug();
        bug2.setBugId(2);
        bug2.setDeviceId(2);
        bug2.setTesterId(1);
        Bug bug3 = new Bug();
        bug3.setBugId(3);
        bug3.setDeviceId(1);
        bug3.setTesterId(2);
        Bug bug4 = new Bug();
        bug4.setBugId(4);
        bug4.setDeviceId(3);
        bug4.setTesterId(2);
        List<Bug> resultList = new ArrayList<>();
        resultList.add(bug1);
        resultList.add(bug2);
        resultList.add(bug3);
        resultList.add(bug4);
        return resultList;
    }
}
