package pl.dgrzankowski.testerfinder.dao;

import net.sf.jsefa.common.converter.IntegerConverter;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;

@CsvDataType()
class TestData{

    @CsvField(pos = 1, converterType = IntegerConverter.class)
    private Integer  id;

    @CsvField(pos = 2)
    private String  description;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
