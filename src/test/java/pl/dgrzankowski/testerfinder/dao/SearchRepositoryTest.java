package pl.dgrzankowski.testerfinder.dao;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchRepositoryTest {

    @Autowired
    private SearchRepository searchRepository;

    @Test
    public void shouldReturnTestData(){
        List<TestData> testList = searchRepository.getData(TestData.class, "test/test.csv");
        assertThat(testList.get(0).getDescription()).isEqualTo("test");
    }
}
