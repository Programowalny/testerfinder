package pl.dgrzankowski.testerfinder.controller;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import pl.dgrzankowski.testerfinder.service.SearchService;
import static org.hamcrest.Matchers.containsString;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class SearchControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private SearchService searchService;

    @Test
    public void shouldReturnDefaultViewAndContent() throws Exception {
        when(searchService.getCountryNames()).thenReturn(List.of("Country1"));
        when(searchService.getDevicesNames()).thenReturn(List.of("Device1"));
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk())
                .andExpect(content().string(containsString("Country1")))
                .andExpect(content().string(containsString("Device1")));
    }

    @Test
    public void shouldReturnResultList() throws Exception {
        this.mockMvc.perform(post("/bugs/searches/")).andDo(print()).andExpect(status().isOk())
                .andExpect(view().name("response :: results-block"));
    }

}
