package pl.dgrzankowski.testerfinder.model.criteria;

import java.util.List;

import pl.dgrzankowski.testerfinder.model.dto.SearchCriteriaDTO;
import pl.dgrzankowski.testerfinder.model.dto.BugDTO;

public interface Criteria {

     List<BugDTO> filterBugs(List<BugDTO> bugList, SearchCriteriaDTO searchCriteriaDTO);
}
