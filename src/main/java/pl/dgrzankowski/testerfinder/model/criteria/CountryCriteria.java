package pl.dgrzankowski.testerfinder.model.criteria;

import java.util.List;
import java.util.stream.Collectors;

import pl.dgrzankowski.testerfinder.model.dto.SearchCriteriaDTO;
import pl.dgrzankowski.testerfinder.model.dto.BugDTO;

public class CountryCriteria implements Criteria {

    @Override
    public List<BugDTO> filterBugs(final List<BugDTO> bugList, final SearchCriteriaDTO searchCriteriaDTO) {
        return bugList
                .stream()
                .filter(b -> searchCriteriaDTO.getCountries().contains(b.getTester().getCountry()))
                .collect(Collectors.toList());
    }
}
