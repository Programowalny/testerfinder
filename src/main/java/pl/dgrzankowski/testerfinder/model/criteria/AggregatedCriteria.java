package pl.dgrzankowski.testerfinder.model.criteria;

import java.util.List;


import pl.dgrzankowski.testerfinder.model.dto.SearchCriteriaDTO;
import pl.dgrzankowski.testerfinder.model.dto.BugDTO;

public class AggregatedCriteria implements Criteria {

    private Criteria criteria;
    private Criteria otherCriteria;

    public AggregatedCriteria(Criteria criteria, Criteria otherCriteria){
        this.criteria = criteria;
        this.otherCriteria = otherCriteria;
    }

    @Override
    public List<BugDTO> filterBugs(final List<BugDTO> bugList, final SearchCriteriaDTO searchCriteriaDTO) {
        final List<BugDTO> firstCriteriaItems = criteria.filterBugs(bugList, searchCriteriaDTO);
        final List<BugDTO> secondCriteriaItems = otherCriteria.filterBugs(bugList, searchCriteriaDTO);
        firstCriteriaItems.retainAll(secondCriteriaItems);
        return firstCriteriaItems;
    }
}
