package pl.dgrzankowski.testerfinder.model.domian;

import net.sf.jsefa.common.converter.IntegerConverter;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


@CsvDataType()
public class Device {

    @CsvField(pos = 1, converterType = IntegerConverter.class)
    private Integer  deviceId;
    @CsvField(pos = 2)
    private String  description;

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public int hashCode(){
        return new HashCodeBuilder(17, 37).
                append(deviceId).append(description).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Device device = (Device) obj;
        return new EqualsBuilder()
                .append(deviceId, device.deviceId)
                .append(description, device.description)
                .isEquals();
    }


}
