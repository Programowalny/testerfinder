package pl.dgrzankowski.testerfinder.model.domian;

import net.sf.jsefa.common.converter.IntegerConverter;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

@CsvDataType()
public class Bug {

    @CsvField(pos = 1 , converterType = IntegerConverter.class)
    private Integer bugId;
    @CsvField(pos = 2, converterType = IntegerConverter.class)
    private Integer deviceId;
    @CsvField(pos = 3, converterType = IntegerConverter.class)
    private Integer testerId;

    public Integer getBugId() {
        return bugId;
    }

    public void setBugId(Integer bugId) {
        this.bugId = bugId;
    }

    public Integer getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(Integer deviceId) {
        this.deviceId = deviceId;
    }

    public Integer getTesterId() {
        return testerId;
    }

    public void setTesterId(Integer testerId) {
        this.testerId = testerId;
    }

    @Override
    public int hashCode(){
        return new HashCodeBuilder(17, 37).
                append(bugId).append(deviceId).append(testerId).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Bug bug = (Bug) obj;
        return new EqualsBuilder()
                .append(bugId, bug.bugId)
                .append(deviceId, bug.deviceId)
                .append(testerId, bug.testerId)
                .isEquals();
    }

}
