package pl.dgrzankowski.testerfinder.model.domian;

import net.sf.jsefa.common.converter.IntegerConverter;
import net.sf.jsefa.csv.annotation.CsvDataType;
import net.sf.jsefa.csv.annotation.CsvField;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;


@CsvDataType()
public class Tester {

    @CsvField(pos = 1, converterType = IntegerConverter.class)
    private Integer testerId;
    @CsvField(pos = 2)
    private String firstName;
    @CsvField(pos = 3)
    private String lastName;
    @CsvField(pos = 4)
    private String country;
    @CsvField(pos = 5)
    private String lastLogin;

    public Integer getTesterId() {
        return testerId;
    }

    public void setTesterId(Integer testerId) {
        this.testerId = testerId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }

    @Override
    public int hashCode(){
        return new HashCodeBuilder(17, 37).
                append(testerId).append(firstName).append(lastName).append(lastLogin).append(country).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Tester tester = (Tester) obj;
        return new EqualsBuilder()
                .append(testerId, tester.testerId)
                .append(firstName, tester.firstName)
                .append(lastLogin, tester.lastName)
                .append(lastLogin, tester.lastLogin)
                .append(country, tester.country)
                .isEquals();
    }
}
