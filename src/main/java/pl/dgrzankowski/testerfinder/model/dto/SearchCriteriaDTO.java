package pl.dgrzankowski.testerfinder.model.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.List;

public class SearchCriteriaDTO {

    private List<String> devices;
    private List<String> countries;

    public List<String> getCountries() {
        return countries;
    }

    public void setCountries(List<String> countries) {
        this.countries = countries;
    }

    public List<String> getDevices() {
        return devices;
    }

    public void setDevices(List<String> devices) {
        this.devices = devices;
    }


    @Override
    public int hashCode(){
        return new HashCodeBuilder(17, 37).
                append(devices).append(countries).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        SearchCriteriaDTO searchCriteriaDTO = (SearchCriteriaDTO) obj;
        return new EqualsBuilder()
                .append(devices, searchCriteriaDTO.devices)
                .append(countries, searchCriteriaDTO.countries)
                .isEquals();
    }

}
