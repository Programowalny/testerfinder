package pl.dgrzankowski.testerfinder.model.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import pl.dgrzankowski.testerfinder.model.domian.Device;
import pl.dgrzankowski.testerfinder.model.domian.Tester;

public class BugDTO {

    private Integer bugId;
    private Device device;
    private Tester tester;

    public BugDTO(Integer bugId, Device device, Tester tester) {
        this.bugId = bugId;
        this.device = device;
        this.tester = tester;
    }

    public Integer getBugId() {
        return bugId;
    }

    public Device getDevice() {
        return device;
    }

    public Tester getTester() {
        return tester;
    }

    public void setBugId(Integer bugId) {
        this.bugId = bugId;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public void setTester(Tester tester) {
        this.tester = tester;
    }


    @Override
    public int hashCode(){
        return new HashCodeBuilder(17, 37).
                append(bugId).append(device).append(tester).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        BugDTO bugDTO = (BugDTO) obj;
        return new EqualsBuilder()
                .append(bugId, bugDTO.bugId)
                .append(device, bugDTO.device)
                .append(tester, bugDTO.tester)
                .isEquals();
    }

}
