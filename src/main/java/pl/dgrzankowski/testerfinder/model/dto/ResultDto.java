package pl.dgrzankowski.testerfinder.model.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

public class ResultDto {

    private String name;
    private Integer bugs;

    public ResultDto(String name, Integer bugs) {
        this.name = name;
        this.bugs = bugs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getBugs() {
        return bugs;
    }

    public void setBugs(Integer bugs) {
        this.bugs = bugs;
    }


    @Override
    public int hashCode(){
        return new HashCodeBuilder(17, 37).
                append(name).append(bugs).build();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) { return false; }
        if (obj == this) { return true; }
        if (obj.getClass() != getClass()) {
            return false;
        }
        ResultDto resultDto = (ResultDto) obj;
        return new EqualsBuilder()
                .append(name, resultDto.name)
                .append(bugs, resultDto.bugs)
                .isEquals();
    }
}
