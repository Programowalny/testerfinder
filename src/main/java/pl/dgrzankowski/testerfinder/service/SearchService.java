package pl.dgrzankowski.testerfinder.service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.dgrzankowski.testerfinder.dao.SearchRepository;
import pl.dgrzankowski.testerfinder.model.dto.ResultDto;
import pl.dgrzankowski.testerfinder.model.dto.SearchCriteriaDTO;
import pl.dgrzankowski.testerfinder.model.domian.Bug;
import pl.dgrzankowski.testerfinder.model.dto.BugDTO;
import pl.dgrzankowski.testerfinder.model.domian.Device;
import pl.dgrzankowski.testerfinder.model.domian.Tester;
import pl.dgrzankowski.testerfinder.model.criteria.AggregatedCriteria;
import pl.dgrzankowski.testerfinder.model.criteria.CountryCriteria;
import pl.dgrzankowski.testerfinder.model.criteria.Criteria;
import pl.dgrzankowski.testerfinder.model.criteria.DeviceCriteria;

@Service
public class SearchService {

    private final SearchRepository searchRepository;

    @Autowired
    public SearchService(SearchRepository searchRepository) {
        this.searchRepository = searchRepository;
    }

    public List<String> getDevicesNames() {
        return searchRepository.getDevices().stream().map(Device::getDescription)
                .collect(Collectors.toList());
    }

    public List<String> getCountryNames() {
        return searchRepository.getTesters().stream().map(Tester::getCountry).distinct()
                .collect(Collectors.toList());
    }

    public List<ResultDto> createSearchResult(final SearchCriteriaDTO searchCriteriaDTO){
        final var resultsBugDTOList = getResultsBugsDTO(searchCriteriaDTO);
        final Map<Tester, List<BugDTO>> testerBugListMap = resultsBugDTOList
                .stream()
                .collect(Collectors.groupingBy(BugDTO::getTester));
        final List<ResultDto> resultDTOList = new ArrayList<>();
        for (Map.Entry<Tester, List<BugDTO>> entry : testerBugListMap.entrySet()) {
            resultDTOList.add(this.createResultDTO(entry.getKey(), entry.getValue()));
        }
        resultDTOList.sort(Comparator.comparing(ResultDto::getBugs).reversed());
        return resultDTOList;
    }

    private List<BugDTO> getResultsBugsDTO(final SearchCriteriaDTO searchCriteriaDTO){
        final Criteria countryCriteria = new CountryCriteria();
        final Criteria deviceCriteria = new DeviceCriteria();
        final Criteria criteria = new AggregatedCriteria(countryCriteria, deviceCriteria);
        return criteria.filterBugs(this.createBugsDTO(), searchCriteriaDTO);
    }

    private List<BugDTO> createBugsDTO(){
        final Map<Integer, Device> devicesMap = searchRepository.getDevices()
                .stream()
                .collect(Collectors.toMap(Device::getDeviceId, Function.identity()));
        final Map<Integer, Tester> testerMap = searchRepository.getTesters()
                .stream()
                .collect(Collectors.toMap(Tester::getTesterId, Function.identity()));
        final List<Bug> bugList = searchRepository.getBugs();
        return  bugList
                .stream()
                .map(b-> new BugDTO(b.getBugId(), devicesMap.get(b.getDeviceId()), testerMap.get(b.getTesterId())))
                .collect(Collectors.toList());
    }

    private ResultDto createResultDTO(final Tester tester, final List<BugDTO> bugsDTOList){
        final String testerName = tester.getFirstName().concat(" ").concat(tester.getLastName());
        final int bugsNumber =  bugsDTOList.size();
        return new ResultDto(testerName, bugsNumber);
    }
}
