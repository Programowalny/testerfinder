package pl.dgrzankowski.testerfinder.dao;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Component;

import net.sf.jsefa.Deserializer;
import net.sf.jsefa.common.lowlevel.filter.HeaderAndFooterFilter;
import net.sf.jsefa.csv.CsvIOFactory;
import net.sf.jsefa.csv.config.CsvConfiguration;


@Component
class CsvDataLoader {

    private Logger logger = LoggerFactory.getLogger(CsvDataLoader.class);

     <T> List<T> loadData(Class<T> type , String fileName) {
        List<T> items = new ArrayList<>();

        final CsvConfiguration config = new CsvConfiguration();
        config.setLineFilter(new HeaderAndFooterFilter(1, false, true));
        config.setFieldDelimiter(',');
        final Deserializer deserializer = CsvIOFactory.createFactory(config, type)
                .createDeserializer();

        InputStream is = null;
        try {
            is = new ClassPathResource("/assets/" + fileName).getInputStream();
            deserializer.open(new InputStreamReader(is));
            while (deserializer.hasNext()) {
                T item = deserializer.next();
                items.add(item);
            }
        } catch (IOException e) {
            logger.error(e.getMessage());
        }finally {
            deserializer.close(true);
        }
        return items;
    }
}
