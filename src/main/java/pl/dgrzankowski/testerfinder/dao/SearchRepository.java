package pl.dgrzankowski.testerfinder.dao;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;
import pl.dgrzankowski.testerfinder.model.domian.Bug;
import pl.dgrzankowski.testerfinder.model.domian.Device;
import pl.dgrzankowski.testerfinder.model.domian.Tester;

import java.util.List;

@Repository
public class SearchRepository {

    private static final String DEVICES_CSV = "devices.csv";
    private static final String TESTERS_CSV = "testers.csv";
    private static final String BUGS_CSV = "bugs.csv";

    private final CsvDataLoader csvDataLoader;

    public SearchRepository(CsvDataLoader csvDataLoader) {
        this.csvDataLoader = csvDataLoader;
    }


    @Cacheable(value = "devices")
    public List<Device> getDevices() {
        return getData(Device.class, DEVICES_CSV);
    }

    @Cacheable(value = "testers")
    public List<Tester> getTesters() { return getData(Tester.class, TESTERS_CSV); }

    @Cacheable(value = "bugs")
    public List<Bug> getBugs() {
        return getData(Bug.class, BUGS_CSV);
    }


     <T> List<T> getData(final Class<T> type , final String fileName) {
        return csvDataLoader.loadData(type, fileName);
    }



}
