package pl.dgrzankowski.testerfinder.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import pl.dgrzankowski.testerfinder.model.dto.SearchCriteriaDTO;
import pl.dgrzankowski.testerfinder.service.SearchService;

@Controller
public class SearchController {

    private static final String DEFAULT_PAGE = "index";
    private static final String SEARCH_RESULT ="response :: results-block";

    private final SearchService searchService;


    public SearchController(SearchService searchService) {
        this.searchService = searchService;
    }

    @GetMapping(value = "/")
    public String displayLogin(Model model) {
        model.addAttribute("search", new SearchCriteriaDTO());
        model.addAttribute("allCountries", searchService.getCountryNames());
        model.addAttribute("allDevices", searchService.getDevicesNames());
        return DEFAULT_PAGE;
    }

    @PostMapping(value = "/bugs/searches")
    public String processForm(@ModelAttribute(value = "search") SearchCriteriaDTO searchCriteriaDTO, Model model) {
        model.addAttribute("resultList", searchService.createSearchResult(searchCriteriaDTO));
        return SEARCH_RESULT;
    }

}
