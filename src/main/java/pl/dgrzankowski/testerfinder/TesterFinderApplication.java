package pl.dgrzankowski.testerfinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class TesterFinderApplication {

    public static void main(String[] args) {
        SpringApplication.run(TesterFinderApplication.class, args);
    }

}
