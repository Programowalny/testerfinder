package pl.dgrzankowski.testerfinder;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import pl.dgrzankowski.testerfinder.dao.SearchRepository;

@Component
public class CacheLoader implements ApplicationRunner {

    private SearchRepository searchRepository;

    @Autowired
    public CacheLoader(SearchRepository searchRepository) {
        this.searchRepository = searchRepository;
    }

    public void run(ApplicationArguments args) {
        searchRepository.getDevices();
        searchRepository.getTesters();
        searchRepository.getBugs();
    }
}