
function ajaxSearchCall(){
    var $form = $("#search-form");
    $form.on('submit', function (e) {
        e.preventDefault();
        $.ajax({
            url: $form.attr('action'),
            type: 'post',
            data: $form.serialize(),
            success: function (response) {
                if ($(response).find('.has-error').length) {
                    $("#results-block").html("<label>Unexpected error</label>");
                } else {
                    $("#results-block").replaceWith(response);
                }
            }
        });
    });
}

function checkAllDevices() {
    $("#checkAllDevices").click(function () {
        $('.devices:checkbox').not(this).prop('checked', this.checked);
    });
}

function checkAllCountries() {
    $("#checkAllCountries").click(function () {
        $('.countries:checkbox').not(this).prop('checked', this.checked);
    });
}